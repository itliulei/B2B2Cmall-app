import Vue from 'vue'
import Router from 'uni-simple-router';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/pages/index/index",
      name: 'index'
    },
    {
      path: "/pages/my/my",
      name: 'my'
    },
    {
      path: "/pages/login/login",
      name: 'login'
    },
    // example
    {
      path: '/pages/example/list/list',
      name: 'example-list'
    },
    {
      path: '/pages/example/grid/grid',
      name: 'example-grid'
    },
    {
      path: '/pages/example/swiper/swiper',
      name: 'example-swiper'
    },
    {
      path: '/pages/example/others/others',
      name: 'example-others'
    },
    {
      path: '/pages/example/qrcode/qrcode',
      name: 'example-qrcode'
    },
    {
      path: '/pages/example/drawer/drawer',
      name: 'example-drawer'
    },
    {
      path: '/pages/example/swipe-action/swipe-action',
      name: 'example-swipe-action'
    },
    {
      path: '/pages/example/navbar-top/navbar-top',
      name: 'example-navbar-top'
    },
    {
      path: '/pages/example/formValidation/formValidation',
      name: 'example-formValidation'
    },
    {
      path: '/pages/example/refresh-and-load/refresh-and-load',
      name: 'example-refresh-and-load'
    },
    {
      path: '/pages/example/refresh-load/refresh-load',
      name: 'example-refresh-load'
    },
    {
      path: '/pages/example/card/card',
      name: 'example-card'
    },
    {
      path: '/pages/exception/exception',
      name: 'example-exception'
    },
    {
      path: '/pages/example/maps/maps',
      name: 'example-maps'
    },
	{
	  path:'/pages/mine/mine',
	  name:'pages-mine'
	},
	{
	  path:'/pages/loginin/loginin',
	 name:'pages-loginin'
	 },
	{
		path:'/pages/nearby/nearby',
		name:'pages-nearby'
	},
	{
		path:'/pages/personalinfo/personalinfo',
		name:'pages-personalinfo'
		},
		{
			path:'/pages/stroll/stroll',
		    name:'pages-stroll'
		}, 
	{
		path:'/pages/order/order',
	 name:'pages-order'
	},
	{
		path:'/pages/checkcode/checkcode',
		name:'pages-checkcode'
	},
	{
		path:'/pages/feedback/feedback',
		name:'pages-feedback'
	},
	{
		path:'/pages/product/product',
		name:'pages-product'
	},
	{
		path:'/pages/order/createOrder',
		name:'pages-createOrder'
	},
	{
		path:'/pages/money/pay',
		name:'pages-pay'
	},
	{
		path:'/pages/category/category',
		name:'pages-category'
	},
	{
		path:'/pages/money/paySuccess',
		name:'pages-moneypaySuccess'
	},
	{
		path:'/pages/order/orderinfo',
		name:'pages-orderinfo'
	},
	{
      path: "/pages/storeinfo/storeinfo",
      name: 'storeinfo'
    },
	{
	  path: "/pages/notice/notice",
	  name: 'notice'
	},
	{
	  path: "/pages/order/cartcreateOrder",
	  name: 'cartcreateOrder'
	},
	{
	  path: "/pages/bill/bill",
	  name: 'bill'
	},
	{
		path: "/pages/card/card",
		name: 'card'
	},
	{
		path: "/pages/shoplist/shoplist",
		name: 'shoplist'
	},
	{
		path: "/pages/promoter/index",
		name: 'promoterindex'
	},
	{
		path: "/pages/promoter/promoteList",
		name: 'promoteList'
	},
	{
		path: "/pages/promoter/addShop",
		name: 'promoteraddShop'
	},
	{
		path: "/pages/commendlist/commendlist",
		name: 'commendlists'
	},
	{
		path: "/pages/commend/commend",
		name: 'commend'
	},
	{
		path: "/pages/order/cancelorder",
		name: 'cancelorder'
	},
	{
		path: "/pages/search/search",
		name: 'search'
  },
    {
      path: "/pages/collection/collection",
      name: 'collection'
    },
    {
      path: "/pages/collectshop/collectshop",
      name: 'collectshop'
    },
    {
      path: "/components/collectgoodslist/collectgoodslist",
      name: 'collectgoodslist'
    },
    {
      path: "/pages/collectgoods/collectgoods",
      name: 'collectgoods'
    }
	

	
	
    // { 
    //   path: "/pages/tabbar/tabbar-4/tabbar-4",
    //   name: 'tabbar-4',
    //   other: {
    //     H5Name: ''
    //   },
    //   beforeEnter: (to, from, next) => {
    //     to.other.H5Name = to.query.name;
    //     next();
    //   }
    // },
    // {
    //   path: "/pages/router/router2/router2",
    //   name: 'router2',
    //   beforeEnter: (to, from, next) => {
    //     next({
    //       name: 'router3',
    //       params: {
    //         msg: '我是从router2路由拦截过来的'
    //       }
    //     });
    //   }
    // }
  ]
});

// router.beforeEach((to, from, next) => {
//   // if (to.name == 'tabbar-5') {
//   //   next({
//   //     name: 'router4',
//   //     params: {
//   //       msg: '我拦截了tab5并重定向到了路由4页面上',
//   //     },
//   //     NAVTYPE: 'push'
//   //   });
//   //   //next();
//   // } else {
//   //   if (to.name == 'tabbar-1') {
//   //     next({
//   //       path: '/pages/tabbar/tabbar-3/tabbar-3',
//   //       NAVTYPE: 'pushTab'
//   //     });
//   //   } else {
//   //     next();
//   //   }
//   // }
//   // console.log(to);
//   // console.log(from)
// });
//
//
// router.afterEach((to, from) => {
//   // console.log(to);
//   // console.log(from)
// });

export default router

