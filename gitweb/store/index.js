import Vuex from 'vuex'
import Vue from 'vue'
import {loginByMobile} from '@/api/userApi'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    version: '1.0.0',
    isLogin: false,
    openid: "",
	uploadimage:'http://a.cnrqhm.com/mall-serve/v1/sys/common/uploadImage',
	imageurl:'http://a.cnrqhm.com/mall-serve/v1/sys/common/getImageByUri?imageUri=',
    userLocation: { // 用户信息
      longitude: null, // 经度
      latitude: null, // 纬度
      locationCity: '' // 定位城市
    },
    userInfo: {
      token: '',
      id: '',
      username: '',
	  mobile:'',
      sex:'',
	  createTime:'',
      updateTime:'',
	  password:'',
	  type:'',
	  idCard:'',
	  state:'',
	  isDelete:'',
	  nickName:'',
	  realName:'',
	  imageUri:''
    }
  },
  mutations: {
    SET_USER_INFO: (state, info) => {
      state.userInfo.token = info.access_token;
      state.userInfo.id = info.userInfno.id;
      state.userInfo.username = info.userInfno.username;
      state.userInfo.mobile = info.userInfno.mobile;
      state.userInfo.sex = info.userInfno.sex;
      state.userInfo.createTime = info.userInfno.createTime;
      state.userInfo.updateTime = info.userInfno.updateTime;
	  state.userInfo.password = info.userInfno.password;
	  state.userInfo.type = info.userInfno.type;
	  state.userInfo.idCard = info.userInfno.idCard;
	  state.userInfo.nickName = info.nickName;
	  state.userInfo.realName = info.userInfno.realName;
	  state.userInfo.imageUri = info.userInfno.imageUrl; 
      state.isLogin = true;
    },
    LOGIN_OUT(state) {
      state.isLogin = false;
      state.userInfo.token = '';
      state.userInfo.id = '';
      state.userInfo.username = '';
      state.userInfo.mobile = '';
      state.userInfo.sex = '';
      state.userInfo.createTime = '';
      state.userInfo.updateTime = '';
	  state.userInfo.password = '';
	  state.userInfo.type = '';
	  state.userInfo.idCard = '';
	  state.userInfo.state = '';
	  state.userInfo.isDelete = '';
	  state.userInfo.nickName = '';
	  state.userInfo.realName = '';
	  state.userInfo.imageUri = '';
      state.isLogin = false;
    },
    setOpenid(state, openid) {
      state.openid = openid;
    }
  },
  actions: {
    storeLogin({commit}, userData) {
      return new Promise((resolve, reject) => {
        loginByMobile(userData).then(response => {
			if(response.code==200){
				console.log(response.result,"这是response.result")
				commit('SET_USER_INFO', response.result)
				uni.setStorageSync('Access-Token', response.result.access_token)
				resolve(response)
			}else{
				uni.showToast({title: '验证码错误',icon: 'none'});
			}
        }).catch(error => {
          reject(error)
        })
      })
    },
    storeLogout({commit}) {
      return new Promise((resolve, reject) => {
        commit('LOGIN_OUT')
        uni.clearStorage()
        resolve()
      })
    }
  }
});

export default store
