import Vue from 'vue'
import App from './App'
import store from './store/index'
import fly from "./common/fly"
import router from './router/router'

import Json from './Json' 
const msg = (title, duration=1500, mask=false, icon='none')=>{
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}
const json = type=>{
	//模拟异步请求数据
	return new Promise(resolve=>{
		setTimeout(()=>{
			resolve(Json[type]);
		}, 500)
	})
}

const prePage = ()=>{
	let pages = getCurrentPages();
	let prePage = pages[pages.length - 2];
	// #ifdef H5
	return prePage;
	// #endif
	return prePage.$vm;
};
const tui = {
  // 提示框
  toast(text, duration, success) {
    uni.showToast({
      title: text,
      icon: success ? 'success' : 'none',
      duration: duration || 2000
    })
  },
  // 跳转
  toPage(url) {
    uni.navigateTo({
      url: url
    })
  },
  // 二维码
  px(num) {
    return uni.upx2px(num) + 'px';
  },
  isLogin() {
    return !!uni.getStorageSync('Access-Token')
  },
  log(val) {
    console.log("label -> ", val)
  },
  dateFormat(date, fmt) {
    // "YYYY-mm-dd HH:MM:SS"
    fmt = fmt | 'YYYY-mm-dd'
    let ret;
    let opt = {
      "Y+": date.getFullYear().toString(),        // 年
      "m+": (date.getMonth() + 1).toString(),     // 月
      "d+": date.getDate().toString(),            // 日
      "H+": date.getHours().toString(),           // 时
      "M+": date.getMinutes().toString(),         // 分
      "S+": date.getSeconds().toString()          // 秒
      // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (let k in opt) {
      ret = new RegExp("(" + k + ")").exec(fmt);
      if (ret) {
        fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
      }
    }
    return fmt;
  }
};


Vue.prototype.tui = tui;
Vue.prototype.$request = fly;
Vue.config.productionTip = false;

Vue.prototype.$store = store;
Vue.prototype.$api = {msg, json, prePage};


App.mpType = 'app';

const app = new Vue({
  store,
  ...App,
  router
});
app.$mount();
