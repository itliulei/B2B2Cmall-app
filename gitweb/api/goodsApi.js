import fly from "@/common/fly";

export function getOneGoosInformation(params) {
  return fly.request({
    url: `/goodsMain/app/getOneGoodsInformation`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /goodsMain//app/getGoodsInformation
export function getGoodsInformation(params) {
  return fly.request({
    url: `/goodsMain/app/getGoodsInformation`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// //goodsMain/app/getSkuInfo
export function getSkuInfo(params) {
  return fly.request({
    url: `/goodsMain/app/getSkuInfo`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/orderComment/orderCommentPage  //获取评论分页

export function orderCommentPage(params) {
  return fly.request({
    url: `/user/orderComment/orderCommentPage `,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}

// /user/cart/delete 删除购物车
export function delcart(params) {
  return fly.request({
    url: `/user/cart/delete`,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function getGoodsRecommond(params) {
  return fly.request({
    url: `/users/product_recommend/list`,
    body: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}

//获取商品分类列表
export function getGoodsCategory(params){
	return fly.request({
		url: `/category/app/list`,
		body: params,
		method: "GET",
		headers: {
		  "Content-Type": "application/json"
		}
	});
}