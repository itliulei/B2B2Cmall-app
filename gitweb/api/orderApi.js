import fly from "@/common/fly";

export function createorder(params) {
  return fly.request({
    url: `/user/order/createOrder`,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
  });
}
// /user/order/list
export function orderlist(params) {
  return fly.request({
    url: `/user/order/list`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/order/info
export function orderinfo(params) {
  return fly.request({
    url: `/user/order/info`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/order/updatePaySuccess
export function updatePaySuccess(params) {
  return fly.request({
    url: `/user/order/updatePaySuccess`,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/order/getPersonalOrderCredential   获得二维码接口
export function getPersonalOrderCredential(params) {
  return fly.request({
    url: `/user/order/getPersonalOrderCredential `,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/cart/updateSelected
export function updateSelected(params) {
  return fly.request({
    url: `/user/cart/updateSelected `,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/cart/confirmCreateOrder
export function cartconfirmCreateOrder(params) {
  return fly.request({
    url: `/user/cart/confirmCreateOrder`,
    params: params,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/order/createOrderFromCart 从购物车创建真实订单
export function createOrderFromCart(params) {
  return fly.request({
    url: `/user/order/createOrderFromCart `,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/orderComment/createOrderComment  创建订单评论
export function createOrderComment(params) {
  return fly.request({
    url: `/user/orderComment/createOrderComment  `,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function getOrderIteminfo(params){
  return fly.request({
    url: `/user/order/app/getOrderItemInfList`,
    body: params,
    method: "GEt",
    headers: {
      "Content-Type": "application/json"
    }
  });
}

// /user/order/cancelOrder 取消订单
export function cancelOrder(params) {
  return fly.request({
    url: `/user/order/cancelOrder  `,
    body: params,
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
// /user/order/confirmCreateOrder 非购物车核算订单
export function confirmCreateOrder(params){
  return fly.request({
    url: `/user/order/confirmCreateOrder`,
    body: params,
    method: "GEt",
    headers: {
      "Content-Type": "application/json"
    }
  });
}