import fly from '@/common/fly';

export function getBannerList() {
    return fly.request({
        url: `/users/banner/list`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}
export function getShopRecommond(){
    return fly.request({
        url: `/shoprecommond/app/list`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}
export function getCouponCardTemplateListOfAllShop(){
    return fly.request({
        url: `/users/coupon/template/listAllShop`,
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}