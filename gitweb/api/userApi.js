import fly from '@/common/fly';

export function userLogin(params) {
  const data = {...params, grant_type: 'password'};
  return fly.request({
    url: `/oauth/token`,
    params: data,
    method: 'POST',
    headers: {
      'isToken': false,
      'Authorization': 'Basic Y2hpbGxheC1ib290OmNoaWxsYXgtYm9vdC1zZWNyZXQ=',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}
export function getVerificationCode(params) {
  return fly.request({
    url: `/sys/app/member/getVerificationCode`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
export function loginByMobile(params) {
  return fly.request({
    url: `/sys/app/member/loginByMobile`,
    body: params,
    method: 'POST',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /member/app/updatePersoonalInfo
export function changeuserinfo(params) {
  return fly.request({
    url: `/member/app/updatePersonalInfo`,
    body: params,
    method: 'POST',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /member/app/getPersonalInfo
export function getPersonalInfo(params) {
  return fly.request({
    url: `/member/app/getPersonalInfo`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /user/cart/add
export function addcart(params) {
  return fly.request({
    url: `/user/cart/add`,
    body: params,
    method: 'POST',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /user/cart/list
export function cartlist(params) {
  return fly.request({
    url: `/user/cart/list`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
export function getMessageList(params) {
  return fly.request({
    url: `/member/app/getMessageList`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function getNewMessageList(params) {
  return fly.request({
    url: `/member/app/getNewMessageList`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /users/template/listBySpuId  商品页面获取优惠券
export function listBySpuId(params) {
  return fly.request({
    url: `/users/coupon/template/listBySpuId`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /users/coupon/card/page  用户查看自己的优惠券列表
export function cardpage(params) {
  return fly.request({
    url: `/users/coupon/card/page`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /users/coupon/card/add  领取优惠券
export function addcard(params) {
  return fly.request({
    url: `/users/coupon/card/add`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
// /users/coupon/getPersonalCouponList 获取可用商品优惠券
export function getPersonalCouponList(params) {
  return fly.request({
    url: `/users/coupon/getPersonalCouponList`,
    body: params,
    method: 'POST',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /users/spu/collection 用户收藏商品
export function collection(params) {
  return fly.request({
    url: `/users/spu/collection`,
    body: params,
    method: 'POST',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}

// /user-api/users/favorite/product/hasUserFavorite 判断用户是否收藏该商品
export function hasUserFavorite(params) {
  return fly.request({
    url: `/users/favorite/product/hasUserFavorite`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /user-api/member/app/favoriteShop 用户收藏商店
export function favoriteShop(params) {
  return fly.request({
    url: `/member/app/favoriteShop`,
    body: params,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /user-api/users/favorite/shop/hasUserFavorite 用户是否收藏了该店铺
export function hasUserFavoriteshop(params) {
  return fly.request({
    url: `/users/favorite/shop/hasUserFavorite `,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /user-api/users/favorite/shop/page 店铺收藏列表
export function collectshoplist(params) {
  return fly.request({
    url: ` users/favorite/shop/page `,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /users/favorite/product/page
export function collectgoodslist(params) {
  return fly.request({
    url: ` /users/favorite/product/page`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /users/banner/adList 获取广告列表
export function adList(params) {
  return fly.request({
    url: ` /users/banner/adList `,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}