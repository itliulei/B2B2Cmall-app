import fly from "@/common/fly";
export function getShopTypeList(params) {
  return fly.request({
    url: `/shopType/getShopTypeList`,
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function modifyShopInfo(params) {
  return fly.request({
    url: `/promoters/app/modifyShopInfo`,
    method: "POST",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function getOrderCommmentListBySpuId(params) {
  return fly.request({
    url: `/user/orderComment/orderCommentPage`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function addCouponCard(params) {
  return fly.request({
    url: `/user/coupon/card/add`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function getTemplateListByShopId(params) {
  return fly.request({
    url: `/user/coupon//template/listByShopId`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function getTemplateListBySpuId(params) {
  return fly.request({
    url: `/user/coupon//template/listBySpuId`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function getSellerVerificationCode(params) {
  return fly.request({
    url: `/sys/merchant/seller/getVerificationCode`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}
export function getPromotShopList(params) {
  return fly.request({
    url: `/promoters/app/getPromotShopList`,
    method: "GET",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}

export function postFeedBack(params) {
  return fly.request({
    url: `/member/app/modifyFeedBack`,
    method: "POST",
    body: params,
    headers: {
      "Content-Type": "application/json"
    }
  });
}
