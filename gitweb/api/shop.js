import fly from '@/common/fly';
// /shop/ShopListOfRange
export function ShopListOfRange(params) {
  return fly.request({
    url: `/shop/app/shopListOfRange`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /shop/app/shopTypeList
export function shopList(params) {
  return fly.request({
    url: `/shop/app/shopList`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /shop/merchant/shopDetail   商户端的获取商铺详情（暂时）
export function shopDetail(params) {
  return fly.request({
    url: `/shop/app/shopDetail`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}

// /shopType/getShopTypeList  获取店铺类目
export function getShopTypeList(params) {
  return fly.request({
    url: `/shopType/getShopTypeList`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /shopType/getShopTypeListOfNoTree 获取非树状商店类目列表
export function getShopTypeListOfNoTree(params) {
  return fly.request({
    url: `/shopType/getShopTypeListOfNoTree`,
    params: params,
    method: 'GET',
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}

// /shoptypename/app/list 获得商铺分类模块
export function applist(params) {
  return fly.request({
    url: `/shoptypename/app/list`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /shoprecommond/app/list 推荐店铺
export function shoprecommond(params) {
  return fly.request({
    url: `/shoprecommond/app/list`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /category/app/list  获取商铺条目
export function shopcategorylist(params) {
  return fly.request({
    url: `/category/app/list`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}