//线上环境配置文件
const BASE_URLS_PROD = {
    //默认服务器地址
	DEFAULT_BASE_URL: "http://a.cnrqhm.com/mall-serve/v1",
    // CMS服务器地址
    // CMS_BASE_URL:"http://47.104.73.76",
    //阿里云
    // ALIYUN_OSS_URL:"http://linbo-image.oss-cn-qingdao.aliyuncs.com",
    //升级资源包下载地址
    // DOWNLOAD_APP_URL:"https://linbo-fe.oss-cn-qingdao.aliyuncs.com/android/supply/supplier-latest.wgt",
    //系统版本号
    VERSION_CODE: 1,
    //系统版本名称
    VERSION_NAME: "1.0.0-beta"
}
export default BASE_URLS_PROD;
