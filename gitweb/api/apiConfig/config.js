//配置文件
import BASE_URLS_DEV from './config-dev'
import BASE_URLS_PROD from './config-prod.js'
import BASE_URLS_LOCAL from './config-local.js'
//默认是dev
let BASE_URLS = BASE_URLS_DEV;
//当前环境，支持dev prod 和 local
const environment = "dev";
if (environment === "prod") {
    //生产环境
    BASE_URLS = BASE_URLS_PROD;
} else if (environment === "local") {
    //本地环境
    BASE_URLS = BASE_URLS_LOCAL;
} else {
    //开发环境
    BASE_URLS = BASE_URLS_DEV;
}
export {BASE_URLS, environment};
