//开发环境配置文件
const BASE_URLS_DEV = {
    // 默认服务器地址
    // DEFAULT_BASE_URL: "http://192.168.0.2:8080",
    DEFAULT_BASE_URL: "http://192.168.0.150:8081",
    // DEFAULT_BASE_URL: "http://47.111.170.38:8999",
    // DEFAULT_BASE_URL: "http://210.44.71.43:19999",
    // CMS服务器地址
    // CMS_BASE_URL: "http://192.168.123.143:8080",
    // CMS_BASE_URL: "http://210.44.71.43:58080",
    //生产环境下的CMS服务器地址（43服务器挂了，先用生产环境的）
    // CMS_BASE_URL:"http://47.104.73.76",
    //阿里云
    // ALIYUN_OSS_URL: "http://7326.oss-cn-beijing.aliyuncs.com",
    //升级资源包下载地址
    // DOWNLOAD_APP_URL:"https://linbo-fe.oss-cn-qingdao.aliyuncs.com/supplier-dev.wgt",
    //系统版本号
    VERSION_CODE: 1,
    //系统版本名称
    VERSION_NAME: "1.0.0-beta"
}
export default BASE_URLS_DEV;
