import fly from '@/common/fly';


export function userLogin(params) {
  const data = {...params, grant_type: 'password'};
  return fly.request({
    url: `/oauth/token`,
    params: data,
    method: 'POST',
    headers: {
      'isToken': false,
      'Authorization': 'Basic Y2hpbGxheC1ib290OmNoaWxsYXgtYm9vdC1zZWNyZXQ=',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export function getVerificationCode(params) {
  return  fly.request({
    url: `/sys/merchant/seller/getVerificationCode`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
export function loginByMobile(params){
	return fly.request({
		url: `/sys/merchant/seller/loginByMobile`,
		body:params,
		method: 'POST',
		headers: {
		  'isToken': false,
		  'Authorization': 'Basic Y2hpbGxheC1ib290OmNoaWxsYXgtYm9vdC1zZWNyZXQ='
		}
	})
}
export function updatenickName(params) {
  return  fly.request({
    url: `/seller/merchant/updatePersonalInfo`,///seller/getVerificationCode
    method: 'POST',
	body:params 
  })
}

export function getMessageList(params) {
  return fly.request({
    url: `/seller/merchant/getMessageList`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function getNewMessageList(params) {
  return fly.request({
    url: `/seller/merchant/getNewMessageList`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

// /seller/merchant/getPersonalInfo
export function getPersonalInfo(params) {
  return fly.request({
    url: `/seller/merchant/getPersonalInfo`,
    params: params,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function bindWxOpenId(params) {
  return fly.request({
    url: `/seller/merchant/bindWxOpenId`,
    params: params,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function unbindWxOpenId(){
  return fly.request({
    url: `/seller/merchant/unbindWxOpenId`,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
