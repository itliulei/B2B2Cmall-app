import fly from '@/common/fly';
export function getCashInfoLogList(params) {
  return  fly.request({
    url: `/cash/seller/getCashInfoLogList`,
    method: 'GET',
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
export function getCashInfoLogById(params) {
    return  fly.request({
      url: `/cash/seller/getCashInfoLogById`,
      method: 'GET',
      body:params, 
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
  export function getSellerCashInfoList(params) {
    return  fly.request({
      url: `/cashInfoLog/seller/getSellerCashInfoList`,
      method: 'GET',
      body:params, 
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }
  export function confirmCash(params) {
    return  fly.request({
      url: `/seller/merchant/confirmCash`,
      method: 'POST',
      body:params, 
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }