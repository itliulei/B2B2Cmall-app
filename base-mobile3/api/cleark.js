import fly from '@/common/fly';
export function clerkadd(params) {
  return  fly.request({
    url: `/clerk/merchant/add`,
    method: 'POST',
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
export function getListOfClerk(params) {
  return  fly.request({
    url: `/seller/merchant/getListOfClerk`,
    method: 'GET', 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// /clerk/delete
export function clerkdelete(params) {
  return  fly.request({
    url: `/clerk/merchant/delete`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// seller/merchant/distributionShop
export function distributionShop(params) {
  return  fly.request({
    url: `/seller/merchant/distributionShop`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
