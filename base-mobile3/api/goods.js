import fly from '@/common/fly';
export function getallgoodsinformation(params) {
  return  fly.request({
    url: `/goodsMain/merchant/getGoodsInformation`,
    method: 'GET', 
	params:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}

export function addcategory(params) {
  return  fly.request({
    url: `/category/merchant/add`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// /category/merchant/list
export function getgoodsitemlist(params) {
  return  fly.request({
    url: `/category/merchant/list`,
    method: 'GET', 
	params:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// /category/merchant/delete
export function deletecategory(params) {
  return  fly.request({
    url: `/category/merchant/delete`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	} 
  }) 
}
// /shopType/getGoodsTypeList
export function queryMallCategory(params) {
  return  fly.request({
    url: `/shopType/getGoodsTypeListByShopId`,
    method: 'GET', 
	params:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// /category/merchant/order
export function editgoodsitemsort(params) {
  return  fly.request({
    url: `/category/merchant/order`,
    method: 'POST', 
	body: params,
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// //goodsMain/merchant/add
export function addgoods(params) {
  return  fly.request({
    url: `/goodsMain/merchant/add`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	} 
  }) 
}
// /goodsMain/merchant/delete
export function goodsdel(params) {
  return  fly.request({
    url: `/goodsMain/merchant/delete`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	} 
  }) 
}
// /goodsMain/merchant/goodsOneUpperLower
export function goodsOneUpperLower(params) {
  return  fly.request({
    url: `/goodsMain/merchant/goodsOneUpperLower`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	} 
  }) 
}
// /goodsMain/merchant/getOneGoodsInformation
export function getgoodsinformation(params) {
  return  fly.request({
    url: `/goodsMain/merchant/getOneGoodsInformation`,
    method: 'GET', 
	params:params, 
	headers: {
	  'Content-Type': 'application/json'
	}
  }) 
}
// /goodsMain/merchant/edit
export function editgoods(params) {
  return  fly.request({
    url: `/goodsMain/merchant/edit`,
    method: 'POST', 
	body:params, 
	headers: {
	  'Content-Type': 'application/json'
	} 
  }) 
}