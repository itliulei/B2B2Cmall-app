
import fly from '@/common/fly';
// /user/seller/getOneOrderDetailsById
export function getOneOrderDetailsById(params) {
  return  fly.request({
    url: `/user/order/seller/getOneOrderDetailsById`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /user/order/writeOffOrderCredential  核销订单
export function writeOffOrderCredential(params) {
  return fly.request({
    url: `/user/order/writeOffOrderCredential`,
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
// /user/order/seller/getOrderListOfClerk 店员查看核销的订单
export function getOrderListOfClerk(params) {
  return  fly.request({
    url: `/user/order/seller/getOrderListOfClerk`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /user/order/seller/getOrderListOfSeller 商户查看订单
export function getOrderListOfSeller(params) {
  return  fly.request({
    url: `/user/order/seller/getOrderListOfSeller`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}
// /seller/getSellerPersonalFinanceInfo 商户获得财务信息

export function getSellerPersonalFinanceInfo(params) {
  return  fly.request({
    url: `/seller/getSellerPersonalFinanceInfo`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}

// /seller/getClerkPersonalFinanceInfo 店员获取财务信息
export function getClerkPersonalFinanceInfo(params) {
  return  fly.request({
    url: `/seller/getClerkPersonalFinanceInfo`,///seller/getVerificationCode
    method: 'GET',
	params:params,
	headers: {
	  'Content-Type': 'application/json'
	}
  })
}