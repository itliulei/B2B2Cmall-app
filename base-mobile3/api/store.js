import fly from '@/common/fly';
export function getShopTypeList(params) {
  return fly.request({
    url: `/shopType/getShopTypeList`,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function shopadd(params) {
  return fly.request({
    url: `/shop/merchant/add`,
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function getshoplist(params) {
  return fly.request({
    url: `/shop/merchant/list`,
    method: 'GET',
    params: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function getshopdetail(params) {
  return fly.request({
    url: `/shop/merchant/shopDetail`,
    method: 'GET',
    params: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
///shop/merchant/updateShopInfo
export function updateShopInfo(params) {
  return fly.request({
    url: `/shop/merchant/updateShopInfo`,
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
// /shop/merchant/delete
export function deleteshop(params) {
  return fly.request({
    url: `/shop/merchant/delete`,
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
export function updateShopStatus(params){
  return fly.request({
    url: `/shop/merchant/updateShopStatus`,
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
