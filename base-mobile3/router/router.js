import Vue from 'vue'
import Router from 'uni-simple-router';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/pages/index/index",
      name: 'index'
    },
    {
      path: "/pages/my/my",
      name: 'my'
    },
    {
      path: "/pages/login/login",
      name: 'login'
    },
    // example
    {
      path: '/pages/example/list/list',
      name: 'example-list'
    },
    {
      path: '/pages/example/grid/grid',
      name: 'example-grid'
    },
    {
      path: '/pages/example/swiper/swiper',
      name: 'example-swiper'
    },
    {
      path: '/pages/example/others/others',
      name: 'example-others'
    },
    {
      path: '/pages/example/qrcode/qrcode',
      name: 'example-qrcode'
    },
    {
      path: '/pages/example/drawer/drawer',
      name: 'example-drawer'
    },
	{
	  path: '/pages/finance/finance',
	  name: 'finance'
	},
    {
      path: '/pages/example/swipe-action/swipe-action',
      name: 'example-swipe-action'
    },
    {
      path: '/pages/example/navbar-top/navbar-top',
      name: 'example-navbar-top'
    },
    {
      path: '/pages/example/formValidation/formValidation',
      name: 'example-formValidation'
    },
    {
      path: '/pages/example/refresh-and-load/refresh-and-load',
      name: 'example-refresh-and-load'
    },
    {
      path: '/pages/example/refresh-load/refresh-load',
      name: 'example-refresh-load'
    },
    {
      path: '/pages/example/card/card',
      name: 'example-card'
    },
    {
      path: '/pages/exception/exception',
      name: 'example-exception'
    },
    {
      path: '/pages/example/maps/maps',
      name: 'example-maps'
    },
	{
		path:'/pages/mine/mine',
		name: 'pages-mine'
	},
	{
		path:'/pages/personalinfo/personalinfo',
		name:'pages-personalinfo'
	},
	{
		path:'/pages/setting/setting',
		name:'pages-setting'
	},
	{
		path:'/pages/ClerkManagement/ClerkManagement',
		name:'pages-ClerkManagement'
	},
	{
		path:'/pages/storelist/storelist',
		name:'pages-storelist'
	},
	{
		path:'/pages/addClerk/addClerk',
		name:'pages-addClerk'
	},
	{
		path:'/pages/checkcode/checkcode',
		name:'pages-checkcode'
    },
	{
		path:'/pages/loginin/loginin',
		name:'pages-loginin'
	},
	{
		path:'/pages/storeinfo/storeinfo',
		name:'pages-storeinfo'
	},
    {
      path: "/pages/changenickName/changenickName",
      name: 'pages-changenickName'
    },
	{
		path:"/pages/addOrEditStore/addOrEditStore",
		name:'pages-addOrEditStore'
	},
	{
		path:'/pages/goodsManage/goodsManage',
		name:'pages-goodsManage'
	},
	{
		path:'/pages/goodsitem/goodsitem',
		name:'pages-goodsitem'
	},
	{
		path:'/pages/addgoodsitem/addgoodsitem',
		name:'pages-addgoodsitem'
	},
	{
		path:'/pages/addgoods/addgoods',
		name:'pages-addgoods'
	},
	{
		path:'/pages/EditStore/EditStore',
		name:'pages-EditStore'
	},
	{
		path:'/pages/editgoods/editgoods',
		name:'pages-editgoods'
	},
	{
		path:'/pages/salesuccess/salesuccess',
		name:'pages-salesuccess'
	},
	{
		path:'/pages/cash/cash',
		name:'pages-cash'
	},
	{
		path:'/pages/cashapplication/cashapplication',
		name:'pages-cashapplication'
	},
	{
		path:'/pages/cash/cashlist',
		name:'pages-cashlist'
	},
	{
		path:'/pages/cash/cashinfo',
		name:'pages-cashinfo'
	},
	{
		path:'/pages/notice/notice',
		name:'pages-notice'
	}

    // {
    //   path: "/pages/tabbar/tabbar-4/tabbar-4",
    //   name: 'tabbar-4',
    //   other: {
    //     H5Name: ''
    //   },
    //   beforeEnter: (to, from, next) => {
    //     to.other.H5Name = to.query.name;
    //     next();
    //   }
    // },
    // {
    //   path: "/pages/router/router2/router2",
    //   name: 'router2',
    //   beforeEnter: (to, from, next) => {
    //     next({
    //       name: 'router3',
    //       params: {
    //         msg: '我是从router2路由拦截过来的'
    //       }
    //     });
    //   }
    // }
  ]
});

// router.beforeEach((to, from, next) => {
//   // if (to.name == 'tabbar-5') {
//   //   next({
//   //     name: 'router4',
//   //     params: {
//   //       msg: '我拦截了tab5并重定向到了路由4页面上',
//   //     },
//   //     NAVTYPE: 'push'
//   //   });
//   //   //next();
//   // } else {
//   //   if (to.name == 'tabbar-1') {
//   //     next({
//   //       path: '/pages/tabbar/tabbar-3/tabbar-3',
//   //       NAVTYPE: 'pushTab'
//   //     });
//   //   } else {
//   //     next();
//   //   }
//   // }
//   // console.log(to);
//   // console.log(from)
// });
//
//
// router.afterEach((to, from) => {
//   // console.log(to);
//   // console.log(from)
// });

export default router

