import Vuex from 'vuex'
import Vue from 'vue'
import {loginByMobile} from '@/api/userApi'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    version: '1.0.0',
  uploadimage:'http://a.cnrqhm.com/mall-serve/v1/sys/common/uploadImage',
	imageurl:'http://a.cnrqhm.com/mall-serve/v1/sys/common/getImageByUri?imageUri=',
    isLogin: false,
    openid: "", 
    userLocation: { // 用户信息
      longitude: null, // 经度a
      latitude: null, // 纬度
      locationCity: '' // 定位城市
    },
    userInfo: {
      token: '',
      createTime: '',
      id: '',
      idCard: '',
      imageUri: '',
      isDelete: '',
      mobile: '',
	  nickName:'',
	  password:'',
	  sex:'',
	  state:'',
	  type:'',
	  updateTime:'',
	  username:'',
	  weixinOpenId:''
    }
  },
  mutations: {
    SET_USER_INFO: (state, info) => {
      state.userInfo.token = info.access_token;
      state.userInfo.createTime = info.userInfo.createTime;
      state.userInfo.id = info.userInfo.id;
      state.userInfo.idCard = info.userInfo.idCard;
      state.userInfo.imageUri = info.userInfo.imageUri;
      state.userInfo.isDelete = info.userInfo.isDelete;
      state.userInfo.mobile = info.userInfo.mobile;
	  state.userInfo.nickName = info.userInfo.nickName;
	  state.userInfo.password = info.userInfo.password;
	  state.userInfo.sex = info.userInfo.sex;
	  state.userInfo.state = info.userInfo.state;
	  state.userInfo.type = info.userInfo.type;
	  state.userInfo.updateTime = info.userInfo.updateTime;
      state.userInfo.username = info.userInfo.username;
	  state.userInfo.weixinOpenId = info.userInfo.weixinOpenId;
      state.isLogin = true;
    },
    LOGIN_OUT(state) {
      state.isLogin = false;
      state.userInfo.token = '';
      state.userInfo.createTime = '';
      state.userInfo.id = '';
      state.userInfo.idCard = '';
      state.userInfo.imageUri = '';
      state.userInfo.isDelete = '';
      state.userInfo.mobile = '';
	  state.userInfo.nickName = '';
	  state.userInfo.password = '';
	  state.userInfo.sex = '';
	  state.userInfo.state = '';
	  state.userInfo.type = '';
	  state.userInfo.updateTime = '';
	  state.userInfo.username = '';
	  state.userInfo.weixinOpenId = '';
      state.isLogin = false;
    },
    setOpenid(state, openid) {
      state.openid = openid;
    } 
  },
  actions: {
    storeLogin({commit}, userData) {
      return new Promise((resolve, reject) => {
        loginByMobile(userData).then(response => {
         if(response.code==200){
			 console.log(response,333333333333333)
         	commit('SET_USER_INFO', response.result)
			const Base64=require("js-base64").Base64;
			var str=response.result.access_token.split('.');
			uni.setStorageSync("usertype",JSON.parse(JSON.parse(Base64.decode(str[1])).userInfo).type);
         	uni.setStorageSync('Access-Token', response.result.access_token)
			uni.setStorageSync('Faceimage',response.result.userInfo.imageUri)
			uni.setStorageSync('nickName',response.result.userInfo.nickName)
			uni.setStorageSync('username',response.result.userInfo.username)
			uni.setStorageSync('mobile',response.result.userInfo.mobile)
			uni.setStorageSync('sex',response.result.userInfo.sex)
         	resolve(response)
         }else if(response.code==0){
         	uni.showToast({title: '验证码错误',icon: 'none'});
         }else{
			 uni.showToast({
			 	title:response.message,
				icon:'none'
			 })
		 }
        }).catch(error => { 
          reject(error)
        })
      })
    },
    storeLogout({commit}) {
      return new Promise((resolve, reject) => {
        commit('LOGIN_OUT')
        uni.clearStorage()
        resolve()
      })
    }
  }
});

export default store
