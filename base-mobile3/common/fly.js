import { BASE_URLS } from "@/api/apiConfig/config";

const BaseUrl = BASE_URLS.DEFAULT_BASE_URL;

var Fly = require("flyio/dist/npm/wx");
var fly = new Fly();

fly.config.timeout = 5000;

fly.config.baseURL = BaseUrl;

//添加请求拦截器
fly.interceptors.request.use(request => {
  // 自定义 headers
  const Authorization = uni.getStorageSync("Access-Token");
  // 判断 Authorization 有效性
  const isToken = (request.headers || {}).isToken === false;
  // console.log('Authorization', Authorization);
  if (Authorization && !isToken) {
    if (!request.headers) {
      request.headers = {};
    }
    // 自定义token
    request.headers["X-Access-Token"] = Authorization;
  }

  // 防止缓存
  if (
    request.method === "POST" &&
    request.headers["Content-Type"] !== "multipart/form-data"
  ) {
    request.body = request.body;
  } else if (request.method === "GET") {
    request.params = {
      ...request.params
    };
  }
  return request;

  //打印出请求体
  console.log("请求体", request.body);
  //终止请求
  //var err=new Error("xxx")
  //err.request=request
  //return Promise.reject(new Error(""))

  return request;
});

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
  response => {
    //只将请求结果的data字段返回
    if (!response.data.success) {
    	if(response.data.message == "用户不存在"){
    		
    		uni.navigateTo({
    			url:'/pages/loginin/loginin'
    		})
    	}else{
    		uni.showToast({
    		  title: response.data.message,
    		  icon: "none"
    		});
    		return;
    	}
    	
      
    }
    return response.data;
  },
  err => {
    //发生网络错误后会走到这里
    const code = err.status;
    switch (code) {
      case 401:
        store.isLogin = false;
        uni.showModal({
          title: "尚未登录",
          content: "您尚未登录，是否去登录？",
          showCancel: true,
          success: res => {
            if (res.confirm) {
              uni.redirectTo({
                url: "/pages/login/login"
              });
            } else if (res.cancel) {
              console.log("用户点击取消");
              store.isLogin = false;
              uni.removeStorageSync("Access-Token");
            }
          }
        });
        break;
      case 404:
        console.log(err);
        uni.navigateTo({
          url: "/pages/exception/exception?action=404"
        });
        break;
      case 500:
        if (err.response.data.message == "Token失效，请重新登录") {
			uni.showToast({
				title: '登录失效，请重新登录'
			});
          uni.navigateTo({
            url: "/pages/loginin/loginin"
          });
        } else {
          uni.showToast({
            title: "网络错误",
            icon: "none"
          });
        }
        break;
      default:
        break;
    }
    return err.data;
  }
);

export default fly;
